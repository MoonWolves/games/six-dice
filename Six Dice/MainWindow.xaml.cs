﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;


namespace Six_Dice
{
    public partial class MainWindow : Window
    {
        Dice[] diceArray = Dices.GetDiceArray();
        List<Image> diceImages = new List<Image>();
        static List<Player> players = new List<Player>();
        bool atleastOneThrow = false;

        Player playerOne = new Player();
        Player playerTwo = new Player();

        int courrentPlayer = 0;
        int roundCount = 0;
        string dice = ".png";
        string clickedDice = "_clicked.png";
        string imagePath = @"img/dices/";
        public MainWindow()
        {
            InitializeComponent();
            DiceBtnCollectInit();
            PlayerOneControlsCollectInit();
            PlayerTwoControlsCollectInit();
            players.Add(playerOne);
            players.Add(playerTwo);
            foreach (Player p in players)
            {
                p.UpdatePlayerName();
            }
            
        }

        // Initialisation functions

        public void DiceBtnCollectInit()
        {
            for (int i = 0; i < diceArray.Length; i++)
            {
                diceArray[i] = new Dice();
            }
            diceImages.Add(dice1img);
            diceImages.Add(dice2img);
            diceImages.Add(dice3img);
            diceImages.Add(dice4img);
            diceImages.Add(dice5img);
            diceImages.Add(dice6img);

        }
        public void PlayerOneControlsCollectInit()
        {
            playerOne.Name = "player1"; //tymczasowe rozwiazanie
            playerOne.buttons.Add(player1One);
            playerOne.buttons.Add(player1Two);
            playerOne.buttons.Add(player1Three);
            playerOne.buttons.Add(player1Four);
            playerOne.buttons.Add(player1Five);
            playerOne.buttons.Add(player1Six);
            playerOne.buttons.Add(player1SmallCross);
            playerOne.buttons.Add(player1LargeCross);
            playerOne.buttons.Add(player1SmallTriangle);
            playerOne.buttons.Add(player1LargeTriangle);
            playerOne.buttons.Add(player1Even);
            playerOne.buttons.Add(player1Odd);
            playerOne.buttons.Add(player1ThreeTimesTwo);
            playerOne.buttons.Add(player1TwoTimesThree);
            playerOne.buttons.Add(player1SmallThreeFour);
            playerOne.buttons.Add(player1LargeThreeFour);
            playerOne.buttons.Add(player1Strit);
            playerOne.buttons.Add(player1Set);
            playerOne.buttons.Add(player1Sos);
            playerOne.spareThrowsLabel = player1SpareThrows;
            playerOne.totalPointsLabel = player1TotalPoints;
            playerOne.playerNameLabel = player1Name;
            playerOne.AddThreeThrows();
        }
        public void PlayerTwoControlsCollectInit()
        {
            playerTwo.Name = "player2"; //tymczasowe rozwiazanie
            playerTwo.buttons.Add(player2One);
            playerTwo.buttons.Add(player2Two);
            playerTwo.buttons.Add(player2Three);
            playerTwo.buttons.Add(player2Four);
            playerTwo.buttons.Add(player2Five);
            playerTwo.buttons.Add(player2Six);
            playerTwo.buttons.Add(player2SmallCross);
            playerTwo.buttons.Add(player2LargeCross);
            playerTwo.buttons.Add(player2SmallTriangle);
            playerTwo.buttons.Add(player2LargeTriangle);
            playerTwo.buttons.Add(player2Even);
            playerTwo.buttons.Add(player2Odd);
            playerTwo.buttons.Add(player2ThreeTimesTwo);
            playerTwo.buttons.Add(player2TwoTimesThree);
            playerTwo.buttons.Add(player2SmallThreeFour);
            playerTwo.buttons.Add(player2LargeThreeFour);
            playerTwo.buttons.Add(player2Strit);
            playerTwo.buttons.Add(player2Set);
            playerTwo.buttons.Add(player2Sos);
            playerTwo.spareThrowsLabel = player2SpareThrows;
            playerTwo.totalPointsLabel = player2TotalPoints;
            playerTwo.playerNameLabel = player2Name;
        }

        // Game managment functions

        public void NextPlayer()
        {
            if (courrentPlayer == players.Count - 1)
            {
                courrentPlayer = 0;
            }
            else courrentPlayer++;
            atleastOneThrow = false;
            players[courrentPlayer].AddThreeThrows();
        }
        public void ClearTable(Player p)
        {
            for (int i = 0; i < 19; i++)
            {
                if (!p.GetIsPointSet(i))
                {
                    p.buttons[i].Content = "";
                }
            }
            ClearDices();
        }
        public void ClearDices()
        {
            for (int i = 0; i < 6; i++)
            {
                diceArray[i].DiceOnHand = true;
                diceImages[i].Source = new BitmapImage(new Uri((imagePath + "empty.png"), UriKind.RelativeOrAbsolute));
            }
        }
        public void TakePoint(object sender, RoutedEventArgs e)
        {
            if (atleastOneThrow)
            {
                Button btn = (Button)sender;
                int playerButton = 0;
                for (int i = 0; i < 19; i++)
                {
                    if (btn.Name == players[courrentPlayer].buttons[i].Name)
                    {
                        playerButton = i;
                        break;
                    }
                }
                if (btn.Name == players[courrentPlayer].buttons[playerButton].Name)
                {
                    players[courrentPlayer].ConfirmPoint(playerButton);
                    btn.IsEnabled = false;
                    players[courrentPlayer].UpdatePlayerPoints();
                    ClearTable(players[courrentPlayer]);            
                    if (courrentPlayer == 1)
                    {
                        Rounds.Content = roundCount + 1;
                        roundCount++;
                    }
                    if (IsThisTheEnd())
                    {
                        EndOfTheGame();
                    }
                    NextPlayer();
                }
            }
        }
        public void IsOnHand(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            string name = btn.Name;
            int position = name.Length - 1;
            int number = int.Parse(name[position].ToString()) - 1;
            if (diceArray[number].DiceOnHand == true)
            {
                diceArray[number].DiceOnHand = false;
                diceImages[number].Source = new BitmapImage(new Uri((imagePath + diceArray[number].DiceValue + clickedDice), UriKind.RelativeOrAbsolute));
            }
            else
            {
                diceArray[number].DiceOnHand = true;
                diceImages[number].Source = new BitmapImage(new Uri((imagePath + diceArray[number].DiceValue + dice), UriKind.RelativeOrAbsolute));
            }
        }
        public void DiceRandom(object sender, RoutedEventArgs e)
        {
            if (!IsThisTheEnd() && players[courrentPlayer].SpareThrows > 0)
            {
                for (int i = 0; i < diceArray.Length; i++)
                {
                    if (diceArray[i].DiceOnHand == true)
                    {
                        diceArray[i].DiceValue = RandomGen.Generate();
                        RandomGen.RNGDispose();
                        diceImages[i].Source = new BitmapImage(new Uri((imagePath + diceArray[i].DiceValue + dice), UriKind.RelativeOrAbsolute));
                    }
                }
                atleastOneThrow = true;
                Check.CheckPlayerPoints(players[courrentPlayer]);
                players[courrentPlayer].RemoveOneThrow();
                players[courrentPlayer].UpdatePlayerTable();
            }
        }
        public void NewGame(object sender, RoutedEventArgs e)
        {
            courrentPlayer = 0;
            roundCount = 0;
            Rounds.Content = roundCount + 1;
            for (int i = 0; i < players.Count; i++)
            {
                players[i].ClearPlayer();

            }
            ClearDices();
            atleastOneThrow = false;
            playerOne.AddThreeThrows();
        }
        public void NewGame()
        {
            courrentPlayer = 0;
            roundCount = 0;
            Rounds.Content = roundCount + 1;
            for (int i = 0; i < players.Count; i++)
            {
                players[i].ClearPlayer();

            }
            ClearDices();
            atleastOneThrow = false;
            playerOne.AddThreeThrows();
        }

        public bool IsThisTheEnd()
        {
            if (roundCount == 19)
            {
                return true;
            }
            else return false;
        }
        static public List<Player> GetPlayerList()
        {
            return players;
        }
        public List<Player> SetPlaces(List<Player> ply)
        {
            bool isSorted;
            Player tempPlayer;
            List<Player> toSort = ply;
            do
            {
                tempPlayer = null;
                isSorted = true;
                for (int i = 0; i < toSort.Count()-1; i++)
                {
                    if (toSort[i].SumPoints() < toSort[i+1].SumPoints())
                    {
                        isSorted = false;
                        tempPlayer = toSort[i];
                        toSort[i] = toSort[i + 1];
                        toSort[i + 1] = tempPlayer;
                    }
                }
            } while (!isSorted);
            return toSort;
        }
        public string PlacesToString()
        {
            string text;
            List<Player> playersSorted = SetPlaces(GetPlayerList());
            if (playersSorted[0].SumPoints() == playersSorted[1].SumPoints())
            {
                text = "Remis!";
            }
            else
            {
                string win = "The Winner is      " + playersSorted[0].Name + "     " + playersSorted[0].SumPoints() + " Points ";
                string lose = "The Looser is      " + playersSorted[playersSorted.Count() - 1].Name + "     " + playersSorted[playersSorted.Count() - 1].SumPoints() + " Points ";
                text =  win + "\n" + lose;
            }
            return text + "\n\n\n Nowa Gra ?   [Tak]\n Wyjście z gry ?   [Nie]";
        }
        public void EndOfTheGame()
        {
            MessageBoxResult mbr = MessageBox.Show(PlacesToString(), "Koniec gry", MessageBoxButton.YesNo);
            if (mbr == MessageBoxResult.Yes)
            {
                NewGame();
            }
            if (mbr == MessageBoxResult.No)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }
    }
}
