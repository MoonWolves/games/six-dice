﻿namespace Six_Dice
{
    public class Dice
    {
        public int DiceValue { get; set; }
        public bool DiceOnHand { get; set; }
        public Dice()
        {
            DiceOnHand = true;
        }
    }
}
