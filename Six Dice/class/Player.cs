﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Six_Dice
{
    public class Player
    {
        private Point[] points = new Point[19];
        public List<Button> buttons = new List<Button>();
        public Label spareThrowsLabel, totalPointsLabel, playerNameLabel;
        public Player()
        {
            SpareThrows = 0;
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new Point();
            }
            Name = "player_name";
        }
        public string Name { get; set; }
        public int SpareThrows { get; private set; }


        public void ConfirmPoint(int point)
        {
            points[point].IsNowTrue();
        }
        public void CheckPoint(int point, int value)
        {
            points[point].CheckedValue = value;
        }
        public bool GetIsPointSet(int point)
        {
            return points[point].IsSet;
        }
        public int SumPoints()
        {
            int sum = 0;
            for (int i = 0; i < points.Length; i++)
            {
                sum += points[i].TrueValue;
            }
            return sum;
        }


        public void AddThreeThrows()
        {
            SpareThrows += 3;
            UpdatePlayerThrows();
        }
        public void RemoveOneThrow()
        {
            if (SpareThrows > 0)
            {
                SpareThrows--;
                UpdatePlayerThrows();
            }
        }


        public void UpdatePlayerName()
        {
            //playerNameLabel.Content = Name;
        }
        public void UpdatePlayerTable()
        {
            for (int i = 0; i < points.Length; i++)
            {
                if (points[i].CheckedValue != 0)
                {
                    buttons[i].Content = points[i].CheckedValue;
                }
            }
        }
        public void UpdatePlayerPoints()
        {
            totalPointsLabel.Content = SumPoints();
        }
        public void UpdatePlayerThrows()
        {
            spareThrowsLabel.Content = SpareThrows;
        }
        public void ClearPlayer()
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i].ClearPoint();
                buttons[i].IsEnabled = true;
                buttons[i].Content = "";
                SpareThrows = 0;
                spareThrowsLabel.Content = "0";
                totalPointsLabel.Content = "0";
            }
        }
    }
}
