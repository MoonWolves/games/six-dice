﻿using System;
using System.Security.Cryptography;

namespace Six_Dice
{
    static class RandomGen
    {
        private static RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider();
        public static int Generate()
        {
            byte[] rno = new byte[6];
            rg.GetBytes(rno);
            int randomvalue = BitConverter.ToInt32(rno, 0);
            randomvalue = Math.Abs(randomvalue % 6) + 1;    //getting balues between 1 and 6.

            return randomvalue;
        }
        public static void RNGDispose()
        {
            rg.Dispose();
        }
    }
}
