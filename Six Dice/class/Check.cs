﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
namespace Six_Dice
{
    static class Check
    {
        static Dice[] diceArray = Dices.GetDiceArray();
        static int[] sameDicesArray = new int[7];
        delegate int CheckDices();
        static List<CheckDices> checks = new List<CheckDices>();
        /// can be changed to 6 for optimalalization
        /// <param name="sameDicesArray"></param>
        public static int HowManyDicesIs(int lookedSide)
        {
            int sum = 0;
            foreach (Dice d in diceArray)
            {
                if (d.DiceValue == lookedSide)
                {
                    sum++;
                }
            }
            return sum;
        }
        public static void CountSameDices()
        {
            for (int i = 1; i < sameDicesArray.Length; i++)
            {
                sameDicesArray[i] = HowManyDicesIs(i);
            }
        }       //must be started after each dices randomization
        public static int SumTheDices()
        {
            int sum = 0;
            foreach (Dice d in diceArray)
            {
                sum += d.DiceValue; 
            }
            return sum;
        }
        public static void CheckPlayerPoints(Player p)
        {
            CountSameDices();
            AddAllFunctionsToCollection();
            for (int i = 0; i < 19; i++)
            {
                if (!p.GetIsPointSet(i))
                {
                    p.CheckPoint(i, checks[i]());
                }
            }
        }
        public static void AddAllFunctionsToCollection()
        {

            checks.Add(CheckOne);
            checks.Add(CheckTwo);
            checks.Add(CheckThree);
            checks.Add(CheckFour);
            checks.Add(CheckFive);
            checks.Add(CheckSix);
            checks.Add(SmallCrossCheck);
            checks.Add(LargeCrossCheck);
            checks.Add(SmallTriangleCheck);
            checks.Add(LargeTriangleCheck);
            checks.Add(CheckEven);
            checks.Add(CheckOdd);
            checks.Add(ThreeTimesTwoCheck);
            checks.Add(TwoTimesThreeCheck);
            checks.Add(SmallThreeFourCheck);
            checks.Add(LargeThreeFourCheck);
            checks.Add(StritCheck);
            checks.Add(SetCheck);
            checks.Add(SOSCheck);

        }
        #region 1-6
        public static int CheckOne()
        {
            return 1 * HowManyDicesIs(1);
        }
        public static int CheckTwo()
        {
            return 2 * HowManyDicesIs(2);
        }
        public static int CheckThree()
        {
            return 3 * HowManyDicesIs(3);
        }
        public static int CheckFour()
        {
            return 4 * HowManyDicesIs(4);
        }
        public static int CheckFive()
        {
            return 5 * HowManyDicesIs(5);
        }
        public static int CheckSix()
        {
            return 6 * HowManyDicesIs(6);
        }
        #endregion
        #region Crosses and Triangles
        public static int SmallCrossCheck()
        {
            if (sameDicesArray[1] + sameDicesArray[2] == 4 
             && sameDicesArray[5] + sameDicesArray[6] == 2)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int LargeCrossCheck()
        {
            if (sameDicesArray[1] + sameDicesArray[2] == 2
             && sameDicesArray[5] + sameDicesArray[6] == 4)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int SmallTriangleCheck()
        {
            if (sameDicesArray[1] + sameDicesArray[2] == 3
             && sameDicesArray[3] + sameDicesArray[4] == 2
             && sameDicesArray[5] + sameDicesArray[6] == 1)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int LargeTriangleCheck()
        {
            if (sameDicesArray[1] + sameDicesArray[2] == 1
             && sameDicesArray[3] + sameDicesArray[4] == 2
             && sameDicesArray[5] + sameDicesArray[6] == 3)
            {
                return SumTheDices();
            }
            else return 0;
        }
        #endregion
        #region Even and Odd
        public static int CheckEven()
        {
            bool isAllEven = true;
            foreach (Dice d in diceArray)
            {
                if (d.DiceValue % 2 != 0)
                {
                    isAllEven = false;
                }
            }
            if (isAllEven)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int CheckOdd()
        {
            bool isAllOdd = true;
            foreach (Dice d in diceArray)
            {
                if (d.DiceValue % 2 == 0)
                {
                    isAllOdd = false;
                }
            }
            if (isAllOdd)
            {
                return SumTheDices();
            }
            else return 0;
        }
        #endregion
        #region 2x3 and 3x2
        public static int ThreeTimesTwoCheck()
        {
            int ThreePairs = 0;
            foreach (int sameDice in sameDicesArray)
            {
                if (sameDice == 2)
                {
                    ThreePairs++;
                }
                else if (sameDice == 4)
                {
                    ThreePairs += 2;
                }
            }
            if (ThreePairs == 3)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int TwoTimesThreeCheck()
        {
            int TwoTriads = 0;
            foreach (int sameDice in sameDicesArray)
            {
                if (sameDice == 3)
                {
                    TwoTriads++;
                }
            }
            if (TwoTriads == 2)
            {
                return SumTheDices();
            }
            else return 0;
        }
        #endregion
        #region 3,4
        public static int SmallThreeFourCheck()
        {
            if (sameDicesArray[3] + sameDicesArray[4] == 4
             && sameDicesArray[1] + sameDicesArray[2] == 2)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int LargeThreeFourCheck()
        {
            if (sameDicesArray[3] + sameDicesArray[4] == 4
             && sameDicesArray[5] + sameDicesArray[6] == 2)
            {
                return SumTheDices();
            }
            else return 0;
        }
        #endregion
        #region Strit, Set, SOS
        public static int StritCheck()
        {
            bool AllIsOne = true;
            for (int i = 1; i < sameDicesArray.Length; i++)
            {
                if (sameDicesArray[i] !=1)
                {
                    AllIsOne = false;
                    break;
                }
            }
            if (AllIsOne)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int SetCheck()
        {
            bool IsOneSix = false;
            foreach (int sameDice in sameDicesArray)
            {
                if (sameDice == 6)
                {
                    IsOneSix = true;
                }
            }
            if (IsOneSix)
            {
                return SumTheDices();
            }
            else return 0;
        }
        public static int SOSCheck()
        {
            return SumTheDices();
        }
        #endregion
    }
}
