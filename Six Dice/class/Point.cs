﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Six_Dice
{
    class Point
    {
        public bool IsSet { get; private set; }
        public int TrueValue { get; private set; }
        public int CheckedValue { get; set; }
        public void IsNowTrue()
        {
            TrueValue = CheckedValue;
            IsSet = true;
        }
        public Point()
        {
            IsSet = false;
        }
        public void ClearPoint()
        {
            IsSet = false;
            TrueValue = 0;
            CheckedValue = 0;
        }
    }
}
