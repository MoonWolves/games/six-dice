﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Six_Dice
{
    /// <summary>
    /// Interaction logic for Win.xaml
    /// </summary>
    public partial class Win : Window
    {
        public Win()
        {
            InitializeComponent();
            
            Updater();
        }
        
        public List<Player> SetPlaces(List<Player> players)
        {
            bool isAllSorted = false;
            bool isSorted;
            do
            {
                isSorted = true;
                Player tempPlayer;
                for (int i = 0; i < players.Count(); i++)
                {
                    if (players[i].SumPoints() > players[i + 1].SumPoints())
                    {
                        isSorted = false;
                        tempPlayer = players[i];
                        players[i] = players[i + 1];
                        players[i + 1] = players[i];
                    }
                }
                if (isSorted)
                {
                    isAllSorted = true;
                }
            } while (!isAllSorted);
            return players;
        }
        public void Updater()
        {
            List<Player> playersSeorted = SetPlaces(MainWindow.GetPlayerList());
            Winer.Content = playersSeorted[0].Name;
            WinerPoints.Content = playersSeorted[0].SumPoints();
            Loser.Content = playersSeorted[playersSeorted.Count() - 1].Name;
            LoserPoints.Content = playersSeorted[playersSeorted.Count() - 1].SumPoints();
        }
        
        
        private void NewGame(object sender, RoutedEventArgs e)
        {
            ((MainWindow)this.Owner).NewGame(sender,e);
        }
    }
}
