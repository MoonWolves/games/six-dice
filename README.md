# Six Dice #
Zastrzegam sobie prawa do samej wersji gry i wszystkich zasad rozgrywki.
Jak również zastrzegam sobie prawa do całego kodu w projekcie.

Wszelkie czerpanie korzyści majatkowych korzystająć z mojego pomysłu i kodu jest zabronione.

Daniel Kasprzyk [11.05.2019]


## CEL APLIKACJI ##
Celem aplikacji jest przeniesienie odmiany gry "Five Dice" inaczej "Yahtzee".

Aplikacja symuluje gre w kości, pozwala nam wylosować do 6 kostek w karzdym rzucie.
odkladać kości(stają sie wtedy przyciemnione) co oznacza że nie bedą one zamienione na nowe wylosowane wartości.

Celem pobocznym jest szifowanie umiejętności w praktyce.

## WSTĘP ##
Celem gry jest zdobycie łącznie największej ilości punktów.
Każdy gracz w swojej rundzie rzuca kośćmi i wybiera do którego pola chce zapisać

## ZASADY GRY ##
* Gracze po kliknięciu na kubełek(losowaniu), mogą wybrać: 
  * Zapisanie punktu na tablicy.
  * Odłożenie jakiejś kostki lub wzięcie jej spowrotem (generalnie możemy podjąć decyzję czy chemy losować daną kostkę czy nie).
  * Możemy ponownie wylosować nowe kostki.
* Gracze co ture dostają trzy rzuty kośćmi, rzuty niewykorzystane nie przepadają i można użyc w kolejnych turach (zapasy).
* Gracz musi przynajmniej raz rzucić koścmi zeby zapisać cokolwiek na tablicy punktów.
* Gracz może zapisywac puste pola jeśli nie mamy co zapisać, niezaleznie od tego czy mamy jeszcze rzuty czy nie.
* Tur jest tyle co pól czyli 19.
* Gra kończy się gdy wszyscy użytkownicy mają skreślone wszystkie pola.
* Logika dla poszczególnych pól.
  * Wszystkie pola sumują kości które spełniają warunki.


Symbol  | Warunki
------------- | -------------
1, 2, 3, 4, 5, 6 | wystąpienie danej liczby
&#43; | 4 liczby muszą być 1 lub 2, pozostałe 2 liczby muszą być 5 lub 6
➕ | 4 liczby muszą być 5 lub 6, pozostałe 2 liczby muszą być 1 lub 2
▵ | 3 liczby muszą być 1 lub 2, 2 liczby muszą być 3 lub 4 i pozostała liczba musi być 5 lub 6
△ | 3 liczby muszą być 5 lub 6, 2 liczby muszą być 3 lub 4 i pozostała liczba musi być 5 lub 6
🅟 | Wszystkie liczby muszą być parzyste
🅝 | Wszystkie liczby muszą być nieparzyste
3•2 | Należy uzbierać 3 jakiekolwiek pary liczb
2•3 | Należy uzbierać 2 jakiekolwiek triady liczb
3,4🅼 | 4 liczby muszą być 3 lub 4, pozostałe 2 liczby muszą być 1 lub 2
3,4🅳 | 4 liczby muszą być 3 lub 4, pozostałe 2 liczby muszą być 5 lub 6
Strit | Oznacza posiadania kostek, każda o różnej wartośći reprezentujących wszystkie możliwe strony kości
Komplet | Oznacza że posiadamy 6 kostek o takich samych wartościach
SOS | Dowolna kombinacja kości






